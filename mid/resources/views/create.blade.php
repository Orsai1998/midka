<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Successfully added!</title>
	 <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
</head>
<body>
	<h3 align="center">Successfully added!</h3>
	<div class="container">
	<div class="row">
		<div class="col-md-5"></div>
		<div class="col-md-3">
			 <a href="{{ URL::previous() }}">
			<button class="btn btn-default">
             Go Back
            </button></a>
		</div>
		<div class="col-md-4"></div>
	</div>
</div>


</body>
</html>
