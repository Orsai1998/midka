<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Insert Data</title>
	 <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/main.css" rel="stylesheet">
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-3"></div>
			<div class="col-md-6">
				  <div class="form-group">
                   {!! Form::open(['route' => 'user.insert.store']) !!}
                  {{Form::label('title', 'Title of question')}}
                  {{Form::text('title', null,array('class' => 'form-control'))}}
            </div>
            <div class="form-group">
              {{Form::label('Title', 'Description')}}
              {{Form::textarea('description', null,array('class' => 'form-control'))}}
             </div>
                  {{Form::submit('Save',array('class' => 'btn btn-default'))}}     
                  {!! Form::close() !!}      
                  <a href="<?php echo route('show.show') ?>"><button class="btn btn-default">
                  	Show
                  </button></a>
                
			</div>
			<div class="col-md-3"></div>
		</div>
	</div>
</body>
</html>