
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Questions</title>
	 <link href="css/bootstrap.css" rel="stylesheet">
     <link href="css/main.css" rel="stylesheet">
</head>
<body>
	<div class="container">
  <h2>Questions</h2>          
  <table class="table">
    <thead>
      <tr>
        <th>Questions</th>
        <th>Descriptions</th>
      </tr>
    </thead>
    <tbody>
      <?php 
		   foreach ($laravel as $question){?>
			<tr>
           <td><?php echo ($question -> title); ?></td>
           <td><?php echo ($question -> description); ?></td>
			</tr>

		<?php
	      }
		?>		
    </tbody>
  </table>
</div>
</table>
<div class="container">
	<div class="row">
		<div class="col-md-4"></div>
		<div class="col-md-4">
		 {{ $laravel->links() }}
		</div>
		<div class="col-md-4"></div>
		<div class="col-md-5"></div>
		<div class="col-md-2">
			<a href="{{URL::route('main')}}"><button class="btn btn-default">Insert</button></a>
		</div>
		<div class="col-md-5"></div>
	</div>
</div>

</body>
</html>