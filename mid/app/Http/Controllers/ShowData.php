<?php

namespace App\Http\Controllers;
use App\laravel;
use Illuminate\Http\Request;

use Collective\Html\Eloquent\FormAccessible;    

class ShowData extends Controller
{
    public function show()
    {
    	$laravel=laravel::paginate(2);
    	return view('show',['laravel' => $laravel]);
    }
        
}
