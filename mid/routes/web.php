<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use App\questions;
Route::get('/', function () {
    return view('insert');
});
Route::post('create',[
    'as' => 'user.insert.store',
    'uses' => 'insertData@store'
]);
Route::get('show',[
    'as' => 'show.show',
'uses' => 'ShowData@show']);
Route::patch('/',[
    'as' => 'main']);